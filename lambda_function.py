import json
import sendgrid
import os
import base64
from sendgrid.helpers.mail import *

def lambda_handler(event, context):
    # TODO implement
    print(json.dumps(event))
    sg = sendgrid.SendGridAPIClient(apikey=os.environ.get('SENDGRID_API_KEY'))
    from_email = Email(event['body-json']['from'])
    to_email = Email(event['body-json']['to'])
    subject = event['body-json']['subject']
    content = Content("text/html", event['body-json']['message'])
    create_attach_file(event['context']['request-id'],event['body-json']['attachment'])
    attachment = create_attachment(event['context']['request-id'])
    mail = Mail(from_email, subject, to_email, content)
    mail.add_attachment(attachment)
    response = sg.client.mail.send.post(request_body=mail.get())
    return {
        'statusCode': 200,
        'message': 'ok'
    }

def create_attach_file(request_id, attach_json):
    attach_folder = "/tmp/" + request_id
    if not os.path.exists(attach_folder):
        os.makedirs(attach_folder)
    attach_file = open(attach_folder + "/company_info.json", "w")
    # magic happens here to make it pretty-printed
    attach_file.write(json.dumps(attach_json, indent=4))
    attach_file.close()

def create_attachment(request_id):
    file_path = "/tmp/" + request_id + "/company_info.json"
    with open(file_path,'rb') as f:
        data = f.read()
        f.close()
    encoded = base64.b64encode(data).decode()
    attachment = Attachment()
    attachment.content = encoded
    attachment.type = "application/json"
    attachment.filename = "company_info.json"
    attachment.disposition = "attachment"
    attachment.content_id = "Hello World"
    return attachment
