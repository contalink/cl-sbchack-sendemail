import json
import os
from lambda_function import lambda_handler
import time

def get_event_json():
    with open('request.json') as json_data:
        d = json.load(json_data)
    return d

def set_env_vars():
    os.environ['SENDGRID_API_KEY'] = "hidden"

start = time.time()
set_env_vars()
event = get_event_json()
response = lambda_handler(event, None)
end = time.time()
hours, rem = divmod(end-start, 3600)
minutes, seconds = divmod(rem, 60)
print("{:0>2}:{:0>2}:{:05.2f}".format(int(hours),int(minutes),seconds))
